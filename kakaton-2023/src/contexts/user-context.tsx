import { createContext, useState } from 'react';

interface IUserContext {
    email: string;
    setEmail: (email: string) => void;
    isLoggedIn: boolean;
}

export const UserContext = createContext<IUserContext>({
    email: '',
    setEmail: () => {},
    isLoggedIn: false,
});

export const UserContextProvider = ({ children }: { children: React.ReactNode }) => {
    const [email, setEmail] = useState('');
    const isLoggedIn = email !== '';

    return (
        <UserContext.Provider value={{ email, setEmail, isLoggedIn }}>
            {children}
        </UserContext.Provider>
    );
};
