import { useEffect, useState } from 'react';
import { ChatStream } from '../components/chat-stream';
import { FieldOfStudyCard } from '../components/field-of-study-card';
import { SliderInput } from '../components/slider-input';
import { Course } from '../types/course';
import { useParams } from 'react-router';
import { useConversation } from '../services/conversation-service';
import { useResult } from '../services/result-service';
import { Card } from '../components/card';

// const mockPriorities = ['fizyka', 'informatyka', 'matematyka', 'angielski'];

export const ResultView = () => {
  const { resultId } = useParams();
  const { result } = useResult(resultId);
  const { conversation } = useConversation(result?.conversationId);
  const [prioritiesWithValues, setPrioritiesWithValues] = useState<{ priority: string; value: number; }[]>([]); // [priority, value

  useEffect(() => {
    if (result?.disciplines !== undefined) {
      setPrioritiesWithValues((result.disciplines).map((priority) => ({ priority, value: 100 / result.disciplines.length, })));
    }
  }, [result]);

  // sum of all priorities should be 100, it should lower the value of other priorities if wants to increase one. It should not change order
  const onChange = (value: number, priority: string) => {
    const newPriorities = [...prioritiesWithValues];
    const otherPriorities = newPriorities.filter((p) => p.priority !== priority);
    const sumOfOtherPriorities = otherPriorities.reduce((acc, curr) => acc + curr.value, 0);
    const newOtherPriorities = otherPriorities.map((p) => ({ ...p, value: p.value - ((p.value / sumOfOtherPriorities) * (value - newPriorities.find((p) => p.priority === priority)!.value)) }));
    const newPriority = { ...newPriorities.find((p) => p.priority === priority)!, value };
    const newPriorityIndex = newPriorities.findIndex((p) => p.priority === priority);

    newOtherPriorities.splice(newPriorityIndex, 0, newPriority);
    setPrioritiesWithValues([...newOtherPriorities]);
  };

  const calculateMatch = (course: Course) => {
    let count = 0;
    course.disciplines.forEach((discipline) => {
      const userPriority = prioritiesWithValues.find((p) => p.priority === discipline.name)?.value || 0;
      const coursePriority = parseInt(discipline.percentage) || 0;
      if (coursePriority >= userPriority) {
        count += userPriority / 100;
      } else {
        count += coursePriority / 100;
      }
    });
    return count;
  };

  const sortedByMatch = (courses: Course[]) => {
    return courses.sort((a, b) => calculateMatch(b) - calculateMatch(a));
  };

  return <div>
    <h2 className='text-4xl mb-4 text-center'>Znalezione kierunki</h2>
    <div>
      <h3 className='ml-8 mb-4'>Poniżej znajdują się dyscypliny, które pokrywają Twoje supermoce. Używając suwaków określ w jakim stopniu (0-100) czujesz się pewnie z każdą z nich. Suma wszystkich supermocy zawsze jest 100.</h3>
      <div className='flex flex-wrap gap-2 mb-10 justify-around'>
        {prioritiesWithValues.map((priority, index) => (
          <div className='flex flex-col w-full md:w-5/12' key={`slider-${index}`}>
            <div className='text-gray-600'>{priority.priority}</div>
            <SliderInput value={priority.value} onChange={(value) => onChange(value, priority.priority)} />
          </div>
        ))}
      </div>
    </div>
    <div className='flex gap-4 flex-wrap lg justify-center mb-8'>
      {sortedByMatch(result?.courses?.courses || []).map((course, index) => (
        <FieldOfStudyCard
          key={`course-${index}`}
          schoolName={course.leadingInstitutionName}
          fieldOfStudyName={course.name}
          level={course.level}
          language={course.language}
          disciplines={course.disciplines.map((d) => d.name)}
          city={course.city}
          isStationary={course.form === 'stacjonarne'}
          match={calculateMatch(course) * 100}
        />
      ))}
    </div>
    <h2 className='text-2xl mb-4 text-center'>Historia konwersacji</h2>
    <Card>
      <ChatStream messages={conversation?.messages || []} className='h-full' />
    </Card>
  </div>;
};
