import { Card } from '../components/card';
import { ChatInput } from '../components/chat-input';
import { ChatStream } from '../components/chat-stream';
import { CheckboxesInput } from '../components/checkboxes-input';
import { useParams } from 'react-router-dom';
import { useConversation } from '../services/conversation-service';

export const ChatView = () => {
  const { conversationId } = useParams();
  const { conversation, sendMessageAndMutate } = useConversation(conversationId);

  const showOptions = conversation?.messages !== undefined && conversation?.messages[conversation.messages.length - 1]?.additionalData?.options;

  return (
    <div className='flex flex-col w-full items-center'>
      <h1 className='mb-4 text-xl'>Rozmowa z doradcą AI</h1>
      <Card className='max-w-2xl w-full h-screen-80 flex flex-col justify-between gap-2'>
        <ChatStream messages={conversation?.messages || []} className='h-full' />
        {/* divider */}
        <div className='w-full border-b-2'></div>
        {!showOptions && <ChatInput onSubmit={(text) => sendMessageAndMutate({
          message: text,
          additionalData: conversation?.messages[conversation.messages.length - 1]?.additionalData,
          isUser: true,
        })} />}
        {showOptions && <CheckboxesInput
          onSend={async (value) => {
            await sendMessageAndMutate({
              message: value.filter(val => val.value).map(val => val.name).join(', '),
              isUser: true,
              additionalData: {
                ...conversation?.messages[conversation.messages.length - 1]?.additionalData,
                options: value,
              }
            });
          }}
          options={
            conversation?.messages[conversation.messages.length - 1]?.additionalData?.options || []
          }
          randomNumber={conversation?.messages.length || 0}
        />}
      </Card>
    </div>
  );
};