import { Link } from 'react-router-dom';
import { useResults } from '../services/result-service';

export const ResultsHistoryView = () => {
  const { results } = useResults();

  return (
    <div className='flex flex-col items-center justify-center h-full'>
      <div className='flex flex-col items-center justify-center w-full max-w-md'>
        <h1 className='text-3xl font-bold text-center mb-5'>Historia wyników</h1>
        <div className='flex flex-col gap-2'>
          {(results || []).map(result => (
            <div className='flex flex-col items-center justify-center w-full max-w-md bg-gray-100 rounded-lg p-4'>
              <h2 className='text-xl font-bold text-center mb-5'>{new Date(result.creationDateTime).toLocaleDateString()}</h2>
              <Link to={`/result/${result.resultId}`} target='_blank' className='underline text-blue-500 text-center'>Zobacz wynik</Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};