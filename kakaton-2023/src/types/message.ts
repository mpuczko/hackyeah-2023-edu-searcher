export interface Message {
  message: string;
  additionalData?: {
    options?: { name: string; value: boolean; }[];
    resultId?: string;
  };
  isUser?: boolean;
}