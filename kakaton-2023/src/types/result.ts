import { Course } from './course';

export interface Result {
  id: string;
  courses: { courses: Course[] };
  conversationId: string;
  disciplines: string[];
  creationDateTime: string;
}