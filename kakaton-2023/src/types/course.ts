export interface Course {
  name: string;
  city: string;
  form: string;
  level: string;
  language: string;
  voivodeship: string;
  title: string;
  leadingInstitutionName: string;
  disciplines: {
    name: string;
    percentage: string;
  }[];
}
