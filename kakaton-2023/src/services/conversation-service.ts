import useSWR from 'swr';
import { API_URL } from '../env';
import { Conversation } from '../types/conversation';
import { Message } from '../types/message';

export const createConversation = async (): Promise<Conversation> => {
  const url = `${API_URL}conversation`;
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    }
  });

  return await response.json();
};

export const sendMessage = async (conversationId: string, message: Message): Promise<Message> => {
  const url = `${API_URL}conversation/${conversationId}/message`;
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });

  return await response.json();
};

export const useConversation = (conversationId?: string) => {
  const url = `${API_URL}conversation/${conversationId}`;

  const { data, error, mutate } = useSWR<Conversation>(conversationId ? url : null, (url: string) => fetch(url).then((res) => res.json()));

  const sendMessageAndMutate = async (message: Message) => {
    if (!conversationId) return;

    const conversationWithSentMessage = [...(data?.messages || []), message]
    await mutate(() => sendMessage(conversationId, message)
      .then((newMessage) => {
        return {
          id: conversationId,
          messages: [...conversationWithSentMessage, newMessage],
        };
      }), {
      optimisticData: {
        id: conversationId,
        messages: conversationWithSentMessage,
      },
    });
  };

  return {
    conversation: data,
    error,
    sendMessageAndMutate,
  };
};

