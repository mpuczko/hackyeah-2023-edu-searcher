import useSWR from 'swr';
import { Result } from '../types/result';
import { API_URL } from '../env';

export const useResult = (resultId?: string) => {
  const { data, error } = useSWR<Result>(resultId ? `${API_URL}result/${resultId}` : null, (url: string) => fetch(url).then((res) => res.json()));

  return {
    result: data,
    error,
  };
};

export const useResults = () => {
  const { data, error } = useSWR<{resultId: string, creationDateTime: string}[]>(`${API_URL}result`, (url: string) => fetch(url).then((res) => res.json()));

  return {
    results: data,
    error,
  };
};
