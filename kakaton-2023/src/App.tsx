import './App.css'
import {
  createBrowserRouter,
  Outlet,
  RouterProvider,
} from "react-router-dom";
import { LoginView } from './views/login-view';
import { TitleBar } from './components/title-bar';
import { RegisterView } from './views/register-view';
import { ChatView } from './views/chat-view';
import { ResultView } from './views/result-view';
import { HomeView } from './views/home-view';
import { UserContextProvider } from './contexts/user-context';
import { Footer } from './components/footer';
import { ResultsHistoryView } from './views/results-history-view';

const Root = () => {
  return (
    <div className='min-h-screen flex flex-col'>
      <TitleBar />
      <div className='container lg flex-col flex items-center p-4 mx-auto min-h-view flex-1'>
        <Outlet />
      </div>
      <Footer />
    </div>
  );
};

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      {
        path: "/",
        element: <HomeView />,
      },
      {
        path: "/conversation/:conversationId",
        element: <ChatView />,
      },
      {
        path: "login",
        element: <LoginView />,
      },
      {
        path: "register",
        element: <RegisterView />,
      },
      {
        path: "/result/:resultId",
        element: <ResultView />,
      },
      {
        path: "/results",
        element: <ResultsHistoryView />,
      }
    ],
  },
]);

function App() {

  return (
    <UserContextProvider>
      <RouterProvider router={router} />
    </UserContextProvider>
  );
}

export default App
