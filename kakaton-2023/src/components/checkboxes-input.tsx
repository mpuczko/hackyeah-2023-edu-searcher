import { useEffect, useState } from 'react';
import { Checkbox } from './checkbox';

export const CheckboxesInput = ({ onSend, options, randomNumber }: {
  onSend: (val: { name: string; value: boolean; }[]) => void;
  options: { name: string; value: boolean; }[];
  randomNumber: number;
}) => {
  const [values, setValues] = useState(options);
  const canSend = values.reduce((acc, val) => val.value || acc, false);

  useEffect(() => {
    setValues(options);
  }, [options]);

  const changeAllToTrue = () => {
    const newValues = [...values];
    newValues.forEach((val, index) => {
      newValues[index] = { name: val.name, value: true };
    });
    setValues(newValues);
  };

  return (
    <div className='flex flex-col w-full pr-5 gap-6'>
      <div
        className='flex flex-wrap gap-2'
      >
      {options.map((option, index) => (
          <Checkbox
            onChange={(val) => {
              const newValues = [...values];
              newValues[index] = { name: option.name, value: val };
              setValues(newValues);
            }}
            value={values[index]?.value || false}
            label={option.name}
            key={`checkbox-${randomNumber}-${index}`}
          />
      ))}
      </div>
      {options.length > 4 && <button className='bg-blue-500 hover:bg-blue-600 text-white rounded-lg px-4 py-2 disabled:bg-gray-500 disabled:hover:bg-gray-500'
        onClick={changeAllToTrue}
      >
        Zaznacz wszystkie
      </button>}
      <button className='bg-blue-500 hover:bg-blue-600 text-white rounded-lg px-4 py-2 disabled:bg-gray-500 disabled:hover:bg-gray-500'
        onClick={canSend ? () => onSend(values) : undefined}
        disabled={!canSend}
      >
        Wyślij
      </button>

    </div>
  );
};
