import { Link } from 'react-router-dom';
import { Message } from '../types/message';

export const ChatBubble = ({ message, className, left }: { message: Message; className?: string; left?: boolean; }) => {
  
  return (
    <div className={`flex flex-col mb-2 ${left ? 'items-start' : 'items-end'} ${left ? 'pr-6' : 'pl-6'} ${className}`}>
      <div className={`flex flex-col items-center justify-center px-4 py-2 rounded-lg ${left ? 'bg-gray-600' : 'bg-blue-500'} text-white`}>
        {message.message}
        {message?.additionalData?.resultId && <Link to={`/result/${message.additionalData.resultId}`} className='ml-2 underline text-white'>Zobacz wynik</Link>}
      </div>
    </div>
  );
};
