export const SliderInput = ({ onChange, value, disabled }: { onChange?: (value: number) => void; value: number; disabled?: boolean; }) => {
  return (
    <div className='flex w-full flex-col'>
      <input
        className='flex-1 rounded-lg bg-gray-100 px-4 pt-2 accent-blue-500'
        type='range'
        min={0}
        max={100}
        value={value}
        disabled={disabled}
        onChange={onChange ? (e) => onChange(parseInt(e.target.value)) : undefined}
      />
      <div className='flex justify-between'>
        <div className='m-0 leading-4'>0</div>
        <div className='m-0 leading-4'>100</div>
      </div>
    </div>
  );
}
