export const Footer = () => {
    return (
        <footer className='bg-gray-800 text-white text-center py-4'>
            <p className='text-sm'>
                Made with ❤️ by (MP)^2
            </p>
        </footer>
    );
};